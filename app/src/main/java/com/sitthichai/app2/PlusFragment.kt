package com.sitthichai.app2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.sitthichai.app2.databinding.FragmentPlusBinding

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class PlusFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var _binding : FragmentPlusBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPlusBinding.inflate(inflater, container,false)
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnEqual?.setOnClickListener {
            val num1 = binding?.textinput1plus?.text.toString().toIntOrNull()
            val num2 = binding?.textinput2plus?.text.toString().toIntOrNull()
            val result = num2?.let { num1?.plus(it) }.toString()
            val action = PlusFragmentDirections.actionPlusFragmentToAnswerFragment(result)
            view.findNavController().navigate(action)
        }

    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PlusFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}