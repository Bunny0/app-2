package com.sitthichai.app2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.sitthichai.app2.databinding.FragmentAnswerBinding

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class AnswerFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var _binding : FragmentAnswerBinding? = null
    private val binding get() = _binding
    private lateinit var result: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            result = it.getString(RESULT).toString()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAnswerBinding.inflate(inflater, container, false)
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.textAnswer?.text = result
        binding?.imageAnswer?.setOnClickListener {
            val action = AnswerFragmentDirections.actionAnswerFragmentToHomeFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
    companion object {
        val RESULT = "result"
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AnswerFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}